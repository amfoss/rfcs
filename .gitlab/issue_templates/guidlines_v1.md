# RFC Template
Version 1, April 2020.

* RFC Title : (fill this with the title of the RFC you are proposing)
* Project : (Which project is this RFC associated with, include links to the repository)
* Related MRs : (If any MR has been made concerning this feature add the link for the same)
* Infrastructure : (If this is an infrastructure change mention it here)
* Duration: (How long would it take for the implementation)
* Related Technology(s) : (Android, Web, Blockchain etc...)
* Start Date: (fill with today's date, DD-MM-YYYY)
* License: (fill with what license are you using in this project)

# Summary
[summary]: #summary

One paragraph explanation of the feature.

# Detailed Explanation
[detailed-explanation]: #detailed-explanation

What problem does it solve? What is the proposed solution? How will it be implemented? How will it benefit us?

# Motivation
[motivation]: #motivation

* Why is this change being made and What are its outcomes? 
* Mention at least two use cases.
* Why does this problem matter? How is it affecting us?

# Old pattern
[old-pattern]: #old-pattern

1-2 sentence summary of the old pattern.

# New pattern
[new-pattern]: #new-pattern

1-2 sentence summary of the new pattern.

#  Advantages of switching pattern
[advantage]: #advantage

1. List out the pros.

# Disadvantages of switching pattern
[disadvantage]: #disadvantage

1. List out the cons.

# Impact on codebase
[impact]: #impact

How does this proposal impact the current codebase/infrastructure in question? Is there a chance of this breaking/deprecating something in the codebase if so list out.

# Alternatives
[alternative]: #alternatives

* Apart from the solution being proposed in this RFC, are there other alternative solutions possible?

* What is the impact of not doing this?

# Unresolved Questions
[questions]: #questions

Are there unresolved questions/discussions that cannot be answered currently because they are not in scope yet or due to similar reason(s), please summarise them here.

# Reference Implementation
[reference]: #reference

Link to a branch with reference implementation and 1-2 sentence summary of the same.

---
**NOTE**

Please make sure to provide concise, short sentences in less than 300 words.

---
