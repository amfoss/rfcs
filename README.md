# RFCs for amFOSS and bi0s club.

The [amFOSS](https://amfoss.in) and [bi0s](https://bi0s.in) community has been growing 
exponentially over time. For making the growth in a better way and making 
decisions happening in the club more accountable, we are introducing a new
process flow which streamlines the workflow and improve the FOSS Culture to both clubs.

Many changes, including bug fixes and documentation improvements, can be
implemented and reviewed via the normal GitLab pull request workflow. 
Apart from that, there are many more things which require multiple discussions
and getting a second opinion and as well as third-opinion to reach consensus.

The "RFC" (request for comments) process is intended to provide a consistent
and controlled path for a new method, infrastructure or a major overhaul to the
current system, so that all stakeholders can be confident about the direction 
of the club's evolution.

Some changes though are "substantial", and we ask that these be put through a
bit of a design process and produce a consensus among the community and
the members of both club.


## When you need to follow this process

You need to follow this process if you intend to make "substantial" changes or
improvement to current infrastructure of amFOSS, Team bi0s, bi0s-hardware or the
RFC process itself. What constitutes a "substantial" change is evolving based on
community norms and varies depending on what part of the ecosystem you are 
proposing to change, but may include the following.

  - Introducing and releasing a new project to be part of the Club/Team. 
  - Deprecation of any existing Club/Team's Project.
  - Revamping/redesigning existing Club/Team's Project.
  - Making changes to the club's infrastructure
    like git, mailing list, communication channel etc and others.
      
Some changes do not require an RFC:

  - Rephrasing, reorganizing, refactoring, or otherwise "changing shape does
    not change meaning".
  - Additions that strictly improve objective, numerical quality criteria
    (warning removal, speedup, better platform coverage, more parallelism, trap
    more errors, etc.) of any Club/Team's existing project.
  - Additions are only likely to be _noticed by_ other developer teams or,
    invisible to users-of-the-project.

If you submit a pull request to implement a new feature without going through
the RFC process, it may be closed with a polite request to submit an RFC first.


## Before creating an RFC

A hastily-proposed RFC can hurt its chances of acceptance. Low quality
proposals, proposals for previously-rejected features, or those that don't fit
into the near-term roadmap, may be quickly rejected, which can be demotivating
for the unprepared contributor. Laying some groundwork ahead of the RFC can
make the process smoother.

Although there is no single way to prepare for submitting an RFC, it is
generally, a good idea to pursue feedback from other project developers
beforehand, to ascertain that the RFC may be desirable; having a consistent
impact on the project requires concerted effort toward consensus-building.

The most common preparations for writing and submitting an RFC include talking
the idea over on our IRC Channel(#amfoss, #bi0s) or discussing during Weekly
Meeting or Watercooler meeting or General meetings, posting as pre-RFCs on ML.
You may file issues on this repo for discussion, but these are
not actively looked at by the teams.

As a rule of thumb, receiving encouraging feedback from seniors/alumni's and 
long-standing project developers, and particularly members of the relevant
project is a good indication that the RFC is worth pursuing.


## What the process is

In short, to get a major overhaul to Club/Team's Infrastructure, one must first
get the RFC approved through the Gitlab Issue Board. At that point, the RFC is
"active" and may be implemented with the goal of eventual inclusion into Club/Team.

  1. Create an issue using the templates provided, including at least 2 use-cases.
  1. Share the RFC and discuss it in the Weekly Call of the Team.
  1. Give the team time to get it reviewed.
  1. If there is a strong consensus towards a decision, and it is a [two-way door decision](https://about.gitlab.com/handbook/values/#make-two-way-door-decisions),
      - Label the issue as `will implement` and select a team member to lead the
       implementation of the process and share this with the team,
      - After the implementation is complete, change the label from `will implement`
       to `implemented` and close the issue.
  1. Otherwise, assign the `~need-Asso` issue label. The Alumni and other 
  people including Chief Mentor will review to make the final decision based on
  what is presented in the issue and comment on it with the decision and reasoning,
     - If the decision is to proceed, we should follow the same steps as step 4,
     - If the decision is not to proceed, the issue should be closed. If there 
    is a disagreement, we should still follow things based on [disagree, commit and disagree](https://about.gitlab.com/handbook/values/#disagree-commit-and-disagree).
  1. Before the implementation or closing the issue, the Senior Members will
  give project maintainers a week to raise concerns.


## The RFC life-cycle

Once an RFC becomes "active" then authors may implement it and submit the
feature as a pull request to the respective Project repo/Show demo repo. 
Being "active" is not a rubber stamp, and in particular, still does not mean 
the feature will ultimately be merged; it does mean that in principle all 
the major stakeholders have agreed to the feature and are amenable to merging it.

Furthermore, the fact that a given RFC has been accepted and is "active"
implies nothing about what priority is assigned to its implementation, nor does
it implies anything about whether an amFOSS/bi0s Member has been assigned the 
task of implementing the feature. While it is not *necessary* that the author 
of the RFC also write the implementation, it is by far the most effective way 
to see an RFC through to completion: authors should not expect that other 
project developers will take on responsibility for implementing their accepted
feature.

Modifications to "active" RFCs can be done via related Issue feature in GitLab.
We strive to write each RFC in a manner that will reflect the final design
of the feature; but the nature of the process means that we cannot expect every
merged RFC to reflect what the result will be at the time of the
next major release.

In general, once accepted, RFCs should not be substantially changed. Only very
minor changes should be submitted as amendments. More substantial changes
should be new RFCs, with a note added to the original RFC. Exactly what counts
as a "very minor change" is up to the project developers to decide.

## Reviewing RFCs

While the RFC issue is up, the sub-team may schedule meetings with the
author and/or relevant stakeholders to discuss the issues in greater detail,
and in some cases, the topic may be discussed at a Weekly meeting. In either
case a summary from the meeting will be posted back to the RFC Issue
and to the public channel.

A sub-team makes final decisions about RFCs after the benefits and drawbacks
are well understood. These decisions can be made at any time, but the sub-team
will regularly issue decisions. When a decision is made, the RFC issue will
either be merged or closed. In either case, if the reasoning is not clear from
the discussion in the thread, the sub-team will add a comment describing the
rationale for the decision.


## Implementing an RFC

Some accepted RFCs represent vital features or an important infrastructure that
needs to be implemented with higher priority. Those will be marked with a `high-priority`
label. Other accepted RFCs can represent features that can wait until some
arbitrary developer feels like doing the work. Every accepted RFC has an
associated issue tracking its implementation in the Project's repository; thus
that associated issue can be assigned a priority via the triage process that the
team uses for all issues in the project's repository.

The author of an RFC is not obligated to implement it. Of course, the RFC
author (like any other developer) is welcome to post an implementation for
review after the RFC has been accepted.

If you are interested in working on the implementation for an "active" RFC, but
cannot determine if someone else is already working on it, feel free to ask
(e.g. by leaving a comment on the associated issue or asking us via IRC).


## RFC Postponement

Some RFC pull requests are tagged with the "postponed" label when they are
closed (as part of the rejection process). An RFC closed with "postponed" is
marked as such because we want neither to think about evaluating the proposal
nor about implementing the described feature until some time and will revisit the RFC
at later point of time.


## License

This repository is currently in the process of being licensed under either of:

* Apache License, Version 2.0, ([LICENSE-APACHE](LICENSE-APACHE) or http://www.apache.org/licenses/LICENSE-2.0)
* MIT license ([LICENSE-MIT](LICENSE-MIT) or http://opensource.org/licenses/MIT)


## Contributions

Unless you explicitly state otherwise, any contribution intentionally submitted for inclusion in the work by you, as defined in the Apache-2.0 license, shall be dual licensed as above, without any additional terms or conditions.


_The RFC Process is designed based on RFC Process of [Rust-lang](https://github.com/rust-lang/rfcs) and [Gitlab FrontEnd](https://gitlab.com/gitlab-org/frontend/rfcs/-/tree/master)._
